*** Settings ***
Documentation       Un premier test d'accès à la page de login
Library    SeleniumLibrary

*** Variables ***
${selenium_url}     http://selenium-hub:4444
${url}        https://katalon-demo-cura.herokuapp.com
${browser}    chrome
# ${browser}    firefox
# ${browser}    MicrosoftEdge
${date}       22/06/2022
${comment}    J'ai des acouphènes, trop de musique tue la musique...

*** Keywords ***

Navigate to Cura Website
    # Délai de chargement implicit #
    Set Selenium Implicit Wait    5
    # Ouverture du navigateur sur la page d'acceuil dans la Grid Selenium#
    IF    "${BROWSER}" == "firefox"
        Open Browser    ${url}
        ...    remote_url=${selenium_url}/wd/hub
        ...    desired_capabilities=browserName:${browser}
        # ...    ff_profile_dir=set_preference("intl.accept_languages", "fr-fr")
        # ...    options=add_argument("--headless"); add_argument("--ignore-certificate-errors")
    ELSE
        Open Browser    ${url}
        ...    remote_url=${selenium_url}/wd/hub
        ...    desired_capabilities=browserName:${browser}
        ...    options=add_argument("--headless"); add_argument("--ignore-certificate-errors")
    END
    # Open Browser    ${url}    ${browser}

    # Agrandir la page du navigateur #
    Maximize Browser Window
    Title Should Be    CURA Healthcare Service

Login
    [Arguments]    ${user}    ${password}
    # Acceder à la reservaton d'un rendez-vous #
    Click Element    xpath=//a[@id ='btn-make-appointment']
    # Entrer les user / password #
    Element Should Contain    xpath=//h2[text()='Login']    Login
    Input Text    xpath=//input[@id ='txt-username']    ${user}
    Input Password    xpath=//input[@id ='txt-password']    ${password}
    Click Element    xpath=//button[@id ='btn-login']
    

Book Appointment

    Select From List By Index    xpath=//select[@id='combo_facility']    1
    Select Checkbox    xpath=//input[@id='chk_hospotal_readmission']
    Click Element    xpath=//input[@id='radio_program_medicaid']
    Input Text    //input[@id='txt_visit_date']    ${date}
    Input Text    //textarea[@id='txt_comment']    ${comment}
    Click Element    xpath=//button[@id='btn-book-appointment']

    # Contrôle Réservation #
    Element Should Contain    xpath=//h2[text()='Appointment Confirmation']    Appointment Confirmation
    Element Should Contain    xpath=//p[@id='facility']    Hongkong CURA Healthcare Center
    Element Should Contain    xpath=//p[@id='hospital_readmission']    Yes
    Element Should Contain    xpath=//p[@id='program']    Medicaid
    Element Should Contain    xpath=//p[@id='visit_date']    22/06/2022
    Element Should Contain    xpath=//p[@id='comment']    ${comment}

End of Test and Close Browser

   # Fermer la fenetre du navigateur #
    Close Browser

*** Test Cases ***
Login Page Test with existing account
    Navigate to Cura Website
    Login    John Doe    ThisIsNotAPassword
    # Faire une reservation #
    Element Should Contain    xpath=//h2[text()='Make Appointment']    Make Appointment
    End of Test and Close Browser

Login Page Test with wrong account
    Navigate to Cura Website
    Login    j2ee    j2ee
    Element Should Contain    xpath=//p[@class='lead text-danger']    Login failed!
    End of Test and Close Browser

Book Appointment Test
    Navigate to Cura Website
    Login    John Doe    ThisIsNotAPassword
    Book Appointment
    End of Test and Close Browser